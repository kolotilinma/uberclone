//
//  Utilities.swift
//  UberClone
//
//  Created by Михаил Колотилин on 30.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class Utilities {
    
    
    func attributedButton(_ firstPart: String, _ secondPart: String) -> UIButton {
        let button = UIButton(type: .system)
        let attributedTitle = NSMutableAttributedString(string: firstPart,
                                                        attributes: [.font: UIFont.systemFont(ofSize: 16) ,
                                                                     .foregroundColor: UIColor.white])
        attributedTitle.append(NSAttributedString(string: secondPart,
                                                  attributes: [.font: UIFont.boldSystemFont(ofSize: 16) ,
                                                               .foregroundColor: UIColor.mainBlueTint]))
        button.setAttributedTitle(attributedTitle, for: .normal)
        return button
    }
}
