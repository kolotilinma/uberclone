//
//  UIColor + Extention.swift
//  UberClone
//
//  Created by Михаил Колотилин on 31.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    static let mainBlueTint = UIColor.rgb(red: 17, green: 154, blue: 237)
    static let backgroundColor = UIColor.rgb(red: 25, green: 25, blue: 25)
    
}

