//
//  UITextField + Extention.swift
//  UberClone
//
//  Created by Михаил Колотилин on 31.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

extension UITextField {
    
    convenience init(withPlaceholder placeholder: String, isSecureTextEntry: Bool = false) {
        self.init()
        self.borderStyle = .none
        self.keyboardAppearance = .dark
        self.isSecureTextEntry = isSecureTextEntry
        self.textColor = .white
        self.font = UIFont.systemFont(ofSize: 16)
        self.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                        attributes: [.foregroundColor: UIColor.lightGray])
    }
    
}
