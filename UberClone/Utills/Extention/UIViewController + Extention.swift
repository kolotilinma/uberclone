//
//  UIViewController + Extention.swift
//  UberClone
//
//  Created by Михаил Колотилин on 05.06.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

extension UIViewController {
    func shouldPresentLoadingView(_ present: Bool, message: String? = nil) {
        if present {
            let loadingView = UIView()
            loadingView.frame = self.view.frame
            loadingView.backgroundColor = .black
            loadingView.alpha = 0
            loadingView.tag = 1
            let indicator = UIActivityIndicatorView(style: .large)
            indicator.center = view.center
            let label = UILabel(text: message, font: UIFont.systemFont(ofSize: 20),
                                textColor: .white, textAlignment: .center)
            label.alpha = 0.87
            self.view.addSubview(loadingView)
            loadingView.addSubview(indicator)
            loadingView.addSubview(label)
            label.centerX(inView: loadingView, topAnchor: indicator.bottomAnchor, paddingTop: 32)
            indicator.startAnimating()
            UIView.animate(withDuration: 0.3) {
                loadingView.alpha = 0.7
            }
        } else {
            view.subviews.forEach { (subview) in
                if subview.tag == 1 {
                    UIView.animate(withDuration: 0.3, animations: {
                        subview.alpha = 0
                    }) { (_) in
                        subview.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    func presentAlertController(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
}
