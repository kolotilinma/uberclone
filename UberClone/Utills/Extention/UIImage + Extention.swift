//
//  UIImage + Extention.swift
//  UberClone
//
//  Created by Михаил Колотилин on 04.06.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit


extension UIImage {
    
    convenience init?(systemName: String, weightConfig: UIImage.SymbolWeight) {
        let boldConfig = UIImage.SymbolConfiguration(weight: weightConfig)
        self.init(systemName: systemName, withConfiguration: boldConfig)
        self.withRenderingMode(.alwaysTemplate)
    }
}
