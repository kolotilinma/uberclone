//
//  LocationInputActivationView.swift
//  UberClone
//
//  Created by Михаил Колотилин on 31.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

protocol LocationInputActivationViewDelegate: class {
    func presentLocationInputView()
}

class LocationInputActivationView: UIView {
    
    //MARK: - Properties
    weak var delegate: LocationInputActivationViewDelegate?
    private let indicatorView:  UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    fileprivate let placeholderLabel: UILabel = {
        let label = UILabel(text: "Where to?", font: UIFont.systemFont(ofSize: 18), textColor: .darkGray)
        return label
    }()
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addShadow()
        
        addSubview(indicatorView)
        indicatorView.centerY(inView: self, leftAnchor: leftAnchor, paddingLeft: 16)
        indicatorView.setDimensions(width: 6, height: 6)
        addSubview(placeholderLabel)
        placeholderLabel.centerY(inView: self, leftAnchor: indicatorView.rightAnchor, paddingLeft: 20)
        let tap = UITapGestureRecognizer(target: self, action: #selector(presentLocationInputView))
        addGestureRecognizer(tap)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Selectors
    @objc func presentLocationInputView() {
        delegate?.presentLocationInputView()
    }
    
}
