//
//  RideActionView.swift
//  UberClone
//
//  Created by Михаил Колотилин on 04.06.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import MapKit

protocol RideActionViewDelegate: class {
    func uploadTrip(_ view: RideActionView)
    func cancelTrip()
}

enum RideActionViewConfiguration {
    case requestRide
    case tripAccepted
    case pickupPassenger
    case tripInProgress
    case endTrip
    
    init() {
        self = .requestRide
    }
}

enum ButtonAction: CustomStringConvertible {
    case requestRide
    case cancel
    case getDirections
    case pickup
    case dropOff
    
    var description: String {
        switch self {
        case .requestRide:   return "CONFIRM UBERX"
        case .cancel:        return "CANCEL RIDE"
        case .getDirections: return "GET DIRECTIONS"
        case .pickup:        return "PICKUP PASSENGER"
        case .dropOff:       return "DROP OFF PASSENGER"
        }
    }
    
    init() {
        self = .requestRide
    }
}

class RideActionView: UIView {

    //MARK: - Properties
    weak var delegate: RideActionViewDelegate?
    var config = RideActionViewConfiguration()
    var buttonAction = ButtonAction()
    var destination: MKPlacemark? {
        didSet {
            titleLabel.text = destination?.name
            addressLabel.text = destination?.address
        }
    }
    var user: User?
    private let titleLabel: UILabel = {
        let label = UILabel(font: UIFont.boldSystemFont(ofSize: 18), textAlignment: .center)
        return label
    }()
    
    private let addressLabel: UILabel = {
        let label = UILabel(font: UIFont.systemFont(ofSize: 16), textColor: .lightGray, textAlignment: .center)
        return label
    }()
    
    private lazy var infoView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        
        view.addSubview(infoViewLabel)
        infoViewLabel.center(inView: view)
        return view
    }()
    
    private let infoViewLabel: UILabel = {
        let label = UILabel(text: "X", font: UIFont.systemFont(ofSize: 30), textColor: .white)
        return label
    }()
    
    private let uberInfoLabel: UILabel = {
        let label =  UILabel(text: "UberX", font: UIFont.boldSystemFont(ofSize: 18), textColor: .black, textAlignment: .center)
        return label
    }()
    
    private let actionButton: UIButton = {
        let button = UIButton(title: "CONFIRM UBERX", titleColor: .white,
                              font: UIFont.boldSystemFont(ofSize: 20), backgroundColor: .black,
                              target: self, action: #selector(actionButtonPressed))
        return button
    }()
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addShadow()
        
        let stack = UIStackView(arrangedSubviews: [titleLabel, addressLabel])
        stack.axis = .vertical
        stack.spacing = 4
        stack.distribution = .fillEqually
        
        addSubview(stack)
        stack.centerX(inView: self, topAnchor: topAnchor, paddingTop: 12)
        addSubview(infoView)
        infoView.centerX(inView: self, topAnchor: stack.bottomAnchor, paddingTop: 16)
        infoView.setDimensions(width: 60, height: 60)
        infoView.layer.cornerRadius = 60 / 2
        addSubview(uberInfoLabel)
        uberInfoLabel.centerX(inView: self, topAnchor: infoView.bottomAnchor, paddingTop: 8)
        let separatorView = UIView()
        separatorView.backgroundColor = .lightGray
        addSubview(separatorView)
        separatorView.anchor(top: uberInfoLabel.bottomAnchor, left: leftAnchor, right: rightAnchor,
                             paddingTop: 6, height: 0.75)
        addSubview(actionButton)
        actionButton.anchor(left: leftAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: rightAnchor,
                            paddingLeft: 12, paddingBottom: 12, paddingRight: 12, height: 50)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Selectors
    @objc func actionButtonPressed() {
        switch buttonAction {
        case .requestRide:
            delegate?.uploadTrip(self)
        case .cancel:
            delegate?.cancelTrip()
        case .getDirections:
            print("DEBUG: Handle get directions")
        case .pickup:
            print("DEBUG: Handle pickup")
        case .dropOff:
            print("DEBUG: Handle drop off")
        }
    }
    
    //MARK: - Helpers
    func configureUI(withCOnfig config: RideActionViewConfiguration) {
        switch config {
        case .requestRide:
            buttonAction = .requestRide
            actionButton.setTitle(buttonAction.description, for: .normal)
        case .tripAccepted:
            guard let user = user else { return }
            if user.accountType == .passenger {
                titleLabel.text = "En Route To Passenger"
                buttonAction = .getDirections
                actionButton.setTitle(buttonAction.description, for: .normal)
            } else {
                buttonAction = .cancel
                actionButton.setTitle(buttonAction.description, for: .normal)
                titleLabel.text = "Driver En Route"
            }
            infoViewLabel.text = String(user.fullname.first ?? "X")
            uberInfoLabel.text = user.fullname
        case .pickupPassenger:
            titleLabel.text = "Arrived At Passenger Location"
            buttonAction = .pickup
            actionButton.setTitle(buttonAction.description, for: .normal)
        case .tripInProgress:
            guard  let user = user else { return }
            if user.accountType == .driver {
                actionButton.setTitle("TRIP IN PROGRESS", for: .normal)
                actionButton.isEnabled = false
            } else {
                buttonAction = .getDirections
                actionButton.setTitle(buttonAction.description, for: .normal)
            }
            titleLabel.text = "En Route To Destination"
        case .endTrip:
            guard  let user = user else { return }
            if user.accountType == .driver {
                actionButton.setTitle("ARRIVED AT DESTINATION", for: .normal)
                actionButton.isEnabled = false
            } else {
                buttonAction = .dropOff
                actionButton.setTitle(buttonAction.description, for: .normal)
            }
            
        }
    }

}
