//
//  LoginController.swift
//  UberClone
//
//  Created by Михаил Колотилин on 30.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import Firebase

class LoginController: UIViewController {
    
    //MARK: - Properties
    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Light", size: 36)
        label.textColor  = UIColor(white: 1, alpha: 0.8)
        label.text = "UBER"
        return label
    }()
    
    fileprivate lazy var emailContainerView: UIView = {
        let view = UIView().inputContainerView(withImage: UIImage(systemName: "envelope")!,
                                                  textField: emailTextField)
        return view
    }()
    
    fileprivate lazy var passwordContainerView: UIView = {
        let view = UIView().inputContainerView(withImage: UIImage(systemName: "lock")!,
                                                  textField: passwordTextField)
        return view
    }()
    
    fileprivate let emailTextField: UITextField = {
        return UITextField(withPlaceholder: "Email")
    }()
    
    fileprivate let passwordTextField: UITextField = {
        return UITextField(withPlaceholder: "Password", isSecureTextEntry: true)
    }()
    
    fileprivate let loginButton: AuthButton = {
        let button  = AuthButton(type: .system)
        button.setTitle("Log In", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    
    fileprivate let dontHaveAccountButton: UIButton = {
        let button = Utilities().attributedButton("Don't have an account ", "Sign Up")
        button.addTarget(self, action: #selector(handleShowSignUp), for: .touchUpInside)
        return button
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Selectors
    @objc func handleLogin() {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print("DEBUG: \(error.localizedDescription)")
                return
            }
            guard let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow}) else { return }
            guard let controller = window.rootViewController as? HomeController else { return }
            controller.configure()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func handleShowSignUp() {
        let controller = SignUpController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: - Helpers
    func setupUI() {
        configureNavigationBar()
        view.backgroundColor = .backgroundColor
        let stack = UIStackView(arrangedSubviews: [emailContainerView, passwordContainerView, loginButton])
        view.addSubview(titleLabel)
        view.addSubview(stack)
        view.addSubview(dontHaveAccountButton)
        stack.axis = .vertical
        stack.spacing = 24
        stack.distribution = .fillEqually
        titleLabel.centerX(inView: view, topAnchor: view.safeAreaLayoutGuide.topAnchor, paddingTop: 60)
        stack.anchor(top: titleLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor,
                     paddingTop: 40, paddingLeft: 32, paddingRight: 32)
        dontHaveAccountButton.anchor(left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                     right: view.rightAnchor, paddingLeft: 40, paddingBottom: 6, paddingRight: 40)
    }
    
    func configureNavigationBar() {
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
    }
    
}
