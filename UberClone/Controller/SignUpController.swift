//
//  SignUpController.swift
//  UberClone
//
//  Created by Михаил Колотилин on 31.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import Firebase
import GeoFire

class SignUpController: UIViewController {
    
    //MARK: - Properties
    private var location = LocationHandler.shared.locationManager.location
    
    fileprivate let titleLabel: UILabel = {
        let label = UILabel(text: "UBER", font: UIFont(name: "Avenir-Light", size: 36),
                            textColor: UIColor(white: 1, alpha: 0.8))
        return label
    }()
    
    fileprivate lazy var emailContainerView: UIView = {
        let view = UIView().inputContainerView(withImage: UIImage(systemName: "envelope")!,
                                                  textField: emailTextField)
        return view
    }()
    
    fileprivate lazy var fullnameContainerView: UIView = {
        let view = UIView().inputContainerView(withImage: UIImage(systemName: "person")!,
                                                  textField: fullnameTextField)
        return view
    }()
    
    fileprivate lazy var passwordContainerView: UIView = {
        let view = UIView().inputContainerView(withImage: UIImage(systemName: "lock")!,
                                                  textField: passwordTextField)
        return view
    }()
    
    fileprivate lazy var accountTypeContainerView: UIView = {
        let view = UIView().inputContainerView(withImage: UIImage(systemName: "person.crop.square.fill")!,
                                               segmentedControl: accountTypeSegmentedControl)
        return view
    }()
    
    fileprivate let emailTextField: UITextField = {
        return UITextField(withPlaceholder: "Email")
    }()
    
    fileprivate let fullnameTextField: UITextField = {
        return UITextField(withPlaceholder: "Full Name")
    }()
    
    fileprivate let passwordTextField: UITextField = {
        return UITextField(withPlaceholder: "Password", isSecureTextEntry: true)
    }()
    
    fileprivate let accountTypeSegmentedControl: UISegmentedControl = {
        let sc = UISegmentedControl(items: ["Rider", "Driver"])
        sc.layer.borderColor = UIColor(white: 1, alpha: 0.87).cgColor
        sc.selectedSegmentTintColor = UIColor(white: 1, alpha: 0.87)
        sc.layer.borderWidth = 0.5
        
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(white: 1, alpha: 0.87)]
        sc.setTitleTextAttributes(titleTextAttributes, for:.normal)
        
        let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.black]
        sc.setTitleTextAttributes(titleTextAttributes1, for:.selected)
        sc.backgroundColor = .backgroundColor
        sc.selectedSegmentIndex = 0
        return sc
    }()
    
    fileprivate let registrationButton: AuthButton = {
        let button  = AuthButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(handleRegistration), for: .touchUpInside)
        return button
    }()
    
    fileprivate let alreadyHaveAccountButton: UIButton = {
        let button = Utilities().attributedButton("Already have an account ", "Log In")
        button.addTarget(self, action: #selector(handleShowLogin), for: .touchUpInside)
        return button
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Selectors
    
    @objc func handleRegistration() {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        guard let fullname = fullnameTextField.text else { return }
        let accountTypeIndex = accountTypeSegmentedControl.selectedSegmentIndex
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print("DEBUG: \(error.localizedDescription)")
                return
            }
            guard let uid = result?.user.uid else { return }
            let values = ["email" : email,
                          "fullname": fullname,
                          "uid": uid,
                          "accountType": accountTypeIndex] as [String : Any]
            if accountTypeIndex == 1 {
                let geofire = GeoFire(firebaseRef: REF_DRIVER_LOCATIONS)
                guard let location = self.location else { return }
                geofire.setLocation(location, forKey: uid) { (error) in
                    if let error = error {
                        print("DEBUG: \(error.localizedDescription)")
                        return
                    }
                    self.uploadUserDataAndShowHomeController(uid: uid, values: values)
                }
            }
            self.uploadUserDataAndShowHomeController(uid: uid, values: values)
        }
    }
    
    @objc func handleShowLogin() {
        navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Helpers
    func uploadUserDataAndShowHomeController(uid: String, values: [String: Any]) {
        REF_USERS.child(uid).updateChildValues(values) { (error, ref) in
            if let error = error {
                print("DEBUG: \(error.localizedDescription)")
                return
            }
            guard let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow}) else { return }
            guard let controller = window.rootViewController as? HomeController else { return }
            controller.configure()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setupUI() {
        view.backgroundColor = .backgroundColor
        let stack = UIStackView(arrangedSubviews: [emailContainerView, fullnameContainerView, passwordContainerView,
                                                   accountTypeContainerView, registrationButton])
        view.addSubview(titleLabel)
        view.addSubview(stack)
        view.addSubview(alreadyHaveAccountButton)
        stack.axis = .vertical
        stack.spacing = 20
        stack.distribution = .fillProportionally
        titleLabel.centerX(inView: view, topAnchor: view.safeAreaLayoutGuide.topAnchor, paddingTop: 60)
        stack.anchor(top: titleLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor,
                     paddingTop: 40, paddingLeft: 32, paddingRight: 32)
        alreadyHaveAccountButton.anchor(left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                     right: view.rightAnchor, paddingLeft: 40, paddingBottom: 6, paddingRight: 40)
    }
    
}
