//
//  PickupController.swift
//  UberClone
//
//  Created by Михаил Колотилин on 05.06.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import MapKit

protocol PickupControllerDelegate: class {
    func didAcceptTrip(_  trip: Trip)
}

class PickupController: UIViewController {
    
    //MARK: - Properties
    let trip: Trip
    weak var delegate: PickupControllerDelegate?
    private let mapView = MKMapView()
    private let cancelButton: UIButton = {
        let button = UIButton(image: UIImage(systemName: "xmark",
                                             weightConfig: .semibold)!,
                              tintColor: .white,
                              target: self,
                              action: #selector(handleDissmissal))
        return button
    }()
    private let pickupLabel: UILabel = {
        let label = UILabel(text: "Would you like to pickup this passenger?",
                            font: UIFont.systemFont(ofSize: 16),
                            textColor: .white)
        return label
    }()
    private let acceptTripButton: UIButton = {
        let button = UIButton(title: "ACCEPT TRIP",
                              titleColor: .black,
                              font: UIFont.boldSystemFont(ofSize: 20),
                              backgroundColor: .white,
                              target: self, action: #selector(handleAcceptTrip))
        return button
    }()
    
    //MARK: - Lifecycle
    init(trip: Trip){
        self.trip = trip
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureMapView()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - Selectors
    @objc func handleDissmissal() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func handleAcceptTrip() {
        Service.shared.acceptTrip(trip: trip) { (error, ref) in
            self.delegate?.didAcceptTrip(self.trip)
        }
    }
    
    //MARK: - API

    
    //MARK: - Helper
    func configureUI() {
        view.backgroundColor = .backgroundColor
        view.addSubview(cancelButton)
        cancelButton.tintColor = .white
        cancelButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor,
                            paddingTop: 16, paddingLeft: 16)
        view.addSubview(mapView)
        mapView.setDimensions(width: 270, height: 270)
        mapView.layer.cornerRadius = 270 / 2
        mapView.center(inView: view, yConstant: -180)
        view.addSubview(pickupLabel)
        pickupLabel.centerX(inView: view, topAnchor: mapView.bottomAnchor, paddingTop: 36)
        view.addSubview(acceptTripButton)
        acceptTripButton.anchor(top: pickupLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor,
                                paddingTop: 16, paddingLeft: 32, paddingRight: 32, height: 50)
    }
    
    func configureMapView() {
        let region = MKCoordinateRegion(center: trip.pickupCoordinates,
                                        latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(region, animated: false)
        let anno = MKPointAnnotation()
        anno.coordinate = trip.pickupCoordinates
        mapView.addAnnotation(anno)
        mapView.selectAnnotation(anno, animated: true)
    }
    
}
